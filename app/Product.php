<?php

namespace App;

use App\Category;
use App\Characteristic;
use App\CharacteristicItem;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name_ru',
        'name_en',
        'name_pl',
        'slug_ru',
        'slug_en',
        'slug_pl',
        'about_ru',
        'about_en',
        'about_pl',
        'category_id',
        'model_url',
        'image'
    ];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }

    public function scopeFilter($query, $filter)
     {
         return $query
             ->when($id = array_get($filter, 'category_id'), function ($query) use ($id) {
                 $ids = Category::where('parent_id', $id)->pluck('id')->push($id);
                 return $query->whereIn('category_id', $ids);
             })
             ->when($sort = array_get($filter, 'sort'), function ($query) use ($sort) {
                 switch ($sort) {
                     case 'name':
                       return $query->orderBy('name');
                     default:
                         return $query;
                 }
             })
         ;
    }
}
