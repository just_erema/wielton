<?php 

namespace App\Http\Middleware;
use Carbon\Carbon;
use Closure;
use App;
use Config;
use Cookie;
use Session;

class Locale {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Cookie::get('langName', 'pl');

        App::setLocale($locale);

        return $next($request);
    }
}