<?php

namespace App\Http\Controllers\Index;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookController extends Controller {

	public function allBooks() {
		return Product::select('id', 'name', 'about_author as author', 'image')->get();
	}

	public function promotions() {
		return Product::select('id', 'name', 'about_author as author', 'image')
						->where('is_promotion', true)
						->get();
	}

	public function new_books() {
		return Product::select('id', 'name', 'about_author as author', 'image')
						->where('is_new', true)
						->orderBy('created_at')
						->get();		
	}

}