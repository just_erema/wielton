<?php

namespace App\Http\Controllers\Index;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App;

class HomeController extends Controller
{
    public function index()
    {
        $categories = Category::with('children')->with('products')->whereNull('parent_id')->orderBy('name_ru')->get();
        $slider = Category::with('children', 'products')->where('inSlider', true)->orderBy('name_ru')->get();
        return view('index.main', compact('categories','slider'));
    }

    public function product($slug) {
        $locale = App::getLocale();
        $category =  Category::with('children')->where('slug_' . $locale, $slug)->first();
        
        if (!$category) {
            return redirect('/');
        }

        $categories = Category::with('children')->whereNull('parent_id')->orderBy('name_' . $locale)->get();
        $products = Product::where('category_id', $category->id)->orderBy('name_' . $locale)->get();
        $subcategories =  Category::with('products')->where('parent_id', $category->id)->orderBy('name_' . $locale)->get();

        if (count($subcategories) > 0) {
            return view('index.parts', compact('categories','category','subcategories','products'));
        } else {
            return view('index.products', compact('products','category','categories'));
        }
    }
}
