<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{

    public function send(Request $request){
        $product_name = $request->input('product', '');
        $title = $product_name ? ('WILLOW | Заявка на ' . $product_name) : 'WILLOW | Новая заявка';
        $name = $request->input('name', '');
        $email = $request->input('email', '');
        $number = $request->input('number', '');
        $text = $request->input('text', '');
        $time = $request->input('time', '');

        Mail::send(
            'emails.order',
            [
                'title' => $title,
                'name' => $name,
                'email' => $email,
                'number' => $number,
                'text' => $text,
                'time' => $time
            ],
            function ($message) use ($title)
            {

                $message->from('willow@willowby.pl', $title);

                $message->to('willow@willowby.pl')->subject($title);

            }
        );

        return redirect('/')->with('flash_message', 'Ваша заявка принята');

    }
}
