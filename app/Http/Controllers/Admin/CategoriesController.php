<?php 

namespace App\Http\Controllers\Admin;

use App\Category;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller {

    public function getSubCategories(Category $category) {
        $parent_id = $category->id;      
        $children = Category::where('parent_id', $parent_id)
                    ->withCount('children')
                    ->get();
        $freeCategories = Category::withCount('children')
                          ->where('id','<>',$parent_id)
                          ->where(function($query) use ($parent_id) {
                                $query->where('parent_id','<>',$parent_id)
                                      ->orWhereNull('parent_id');
                          })
                          ->get();
        
        return compact('category', 'children', 'freeCategories');
    }

    public function getAllCategories() {
        return Category::get();
    }

	public function create(Request $request) {
        $data = [
            'name_ru' => $request->input('name_ru'),
            'name_en' => $request->input('name_en'),
            'name_pl' => $request->input('name_pl'),
            'slug_ru' => $request->input('slug_ru'),
            'slug_en' => $request->input('slug_en'),
            'slug_pl' => $request->input('slug_pl'),
            'parent_id' => $request->input('parent_id'),
            'inSlider' => $request->input('inSlider')
        ];
        if ($request->hasFile('image')) {
            $data['image'] = $request->image->store('categories', 'public');
        }
        Category::create($data);
		return ['result' => 'success'];
	}

	public function update(Category $category, Request $request)
    {
        $data = [
            'name_ru' => $request->input('name_ru'),
            'name_en' => $request->input('name_en'),
            'name_pl' => $request->input('name_pl'),
            'slug_ru' => $request->input('slug_ru'),
            'slug_en' => $request->input('slug_en'),
            'slug_pl' => $request->input('slug_pl'),
            'parent_id' => $request->input('parent_id'),
            'inSlider' => $request->input('inSlider')
        ];
        if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($category->image);
            $data['image'] = $request->image->store('categories', 'public');
        }        
        $category->update($data);
        return ['result' => 'success'];
    }	

	public function delete($id)
    {
    	$category=Category::where('id', $id)->first();
        $category->delete();
        return ['result' => 'success'];
    }	
    
}