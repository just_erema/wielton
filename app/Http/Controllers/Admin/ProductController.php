<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function index(Request $request)
	{
        return Product::get();
	}

	public function show(Product $product) {
        $product = array_merge($product->toArray(), array('categories' => $product->category));
		return compact('product');
	} 

    public function freeCategories(Product $product) {
        $product_id = $product->id;
        $freeCategories = Category::leftJoin('book_categories', function($join) use ($product_id){
            $join->on('categories.id', '=', 'book_categories.category_id');
            $join->on('book_categories.product_id', '=', \DB::raw("'".$product_id."'"));
        })->whereNull('book_categories.category_id')->select('categories.id as id', 'categories.name as name')->get();     
        return $freeCategories->toArray();
    }

    public function categoryAdding($id, Request $request) {
        $product = Product::where('id', $id)->first();
        $category_id = $request->input('category_id');
        $product->category()->attach($category_id);
        return ['result' => $category_id];
    }

	public function create(Request $request) {

        $product = Product::create([
            'name_ru' => $request->input('name_ru'),
            'name_en' => $request->input('name_en'),
            'name_pl' => $request->input('name_pl'),
            'slug_ru' => $request->input('slug_ru'),
            'slug_en' => $request->input('slug_en'),
            'slug_pl' => $request->input('slug_pl'),
            'image' => $request->image->store('products', 'public'), 
            'about_ru' => $request->input('about_ru'),
            'about_en' => $request->input('about_en'),
            'about_pl' => $request->input('about_pl'),
            'model_url' => $request->input('model_url'),
            'category_id' => $request->input('category_id')
        ]);

		return ['result' => 'succsess'];
	}

	public function update($id, Request $request)
    {    
        $data = [
            'name_ru' => $request->input('name_ru'),
            'name_en' => $request->input('name_en'),
            'name_pl' => $request->input('name_pl'),
            'slug_ru' => $request->input('slug_ru'),
            'slug_en' => $request->input('slug_en'),
            'slug_pl' => $request->input('slug_pl'), 
            'about_ru' => $request->input('about_ru'),
            'about_en' => $request->input('about_en'),
            'about_pl' => $request->input('about_pl'),
            'model_url' => $request->input('model_url'),
            'category_id' => $request->input('category_id')
        ];

        $product=Product::where('id', $id)->first();
        if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($product->image);
            $data['image'] = $request->image->store('products', 'public');
        }        
        $product->update($data);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $product=Product::where('id', $id)->first();
        \Storage::disk('public')->delete($product->image);
        $product->delete();

        return ['result' => 'success'];
    }	

    public function deleteCategory(Product $product, Category $category, Request $request) {
        $product->category()->detach($category->id);
        return ['result' => 'success'];
    }

}