<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/send', 'EmailController@send');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AuthController@login');
    Route::post('/', 'AuthController@doLogin');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'api'], function () {
            Route::get('/logout', 'AuthController@logout');
            Route::get('/allCategories', 'CategoriesController@getAllCategories');
            Route::group(['prefix' => 'categories'], function () {
                Route::get('/', 'CategoriesController@getSubCategories');
                Route::get('/{category}', 'CategoriesController@getSubCategories');
                Route::delete('/{id}', 'CategoriesController@delete');
                Route::post('/', 'CategoriesController@create');
                Route::post('/{category}', 'CategoriesController@update');
            });
            Route::group(['prefix' => 'products'], function () {
                Route::get('/', 'ProductController@index');
                // Route::get('/allCategories', 'ProductController@allCategories');
                Route::get('/{product}', 'ProductController@show');
                Route::get('/{product}/free_categories', 'ProductController@freeCategories');
                Route::get('/{product}/comments', 'CommentsController@index');
                // Route::get('/{product}/charcteristics', 'CharacteristicsController@index');
                Route::post('/', 'ProductController@create');
                Route::post('/{product}', 'ProductController@update');
                Route::post('/{product}/categories', 'ProductController@categoryAdding');
                Route::delete('/{product}', 'ProductController@delete');
                Route::delete('/{product}/category/{category}', 'ProductController@deleteCategory');
            }); 
            Route::post('/comments/{comment}', 'CommentsController@update');
            Route::group(['prefix' => 'news'], function () {
                Route::get('/', 'NewsController@index');
                Route::get('/{news}', 'NewsController@show');
                Route::post('/', 'NewsController@create');
                Route::post('/{news}', 'NewsController@update');
                Route::delete('/{news}', 'NewsController@delete');
            });
            Route::group(['prefix' => 'promotions'], function () {
                Route::get('/', 'PromotionsController@index');
                Route::post('/{promotion}', 'ProductController@update');
            });
            Route::group(['prefix' => 'new_books'], function () {
                Route::get('/', 'NewBookController@index');
                Route::post('/{new_books}', 'ProductController@update');
            }); 
            Route::group(['prefix' => 'characteristics'], function () {
                Route::get('/', 'CharacteristicsController@index');
                Route::get('/{characteristic}', 'CharacteristicsController@show');
                Route::post('/', 'CharacteristicsController@create');
                Route::post('/{characteristic}', 'CharacteristicsController@update');
                Route::delete('/{characteristic}', 'CharacteristicsController@delete');
            });          
        });

        Route::any('{all?}', function () {
            return view('admin.main');
        })->where(['all' => '.*']);
    });
});

Route::group(['namespace' => 'Index'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/{slug}', 'HomeController@product');
});

Route::get('locale/{locale?}',
    [
        'as' => 'locale.setlocale',
        'uses' => 'LocaleController@setLocale'
    ]);

