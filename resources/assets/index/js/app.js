import 'babel-polyfill'
import Vue from 'vue'
import axios from 'axios'
require('./bootstrap');
Vue.component('search', require('./components/search_engine/search.vue'));
Vue.component('item-slider', require('./components/itemSlider.vue'));

Vue.prototype.$http = {
    get(url, data) {
        return axios.get(`/${url}`, { params: data }).then(response => response.data)
    },

    post(url, data) {
        return axios.post(`/${url}`, data).then(response => response.data)
    },

    delete (url) {
        return axios.delete(`/${url}`).then(response => response.data)
    },
}

var app = new Vue({
   el: '#app',
   components: {
       'search': require('./components/search_engine/search.vue'),
       'item-slider': require('./components/itemSlider.vue')
   }
});
