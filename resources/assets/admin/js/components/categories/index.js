import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/categories',
        component: Layout,
        props: {
            partName: 'Категории',
            icon: 'sort',
        },   
        children: [
            {
                path: '', 
                name: 'Список категорий',
                component: Table, 
                props: {
                    partName: 'Список категорий',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание категории',
                component: Create,
                props: {
                    partName: 'Создание категории',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование категории',
                component: Edit, 
                props: {
                    partName: 'Редактирование категории',
                    icon: 'edit',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]