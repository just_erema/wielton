@extends('index.layout')

@section('head')
  <title>WILLOW | {{ $category['name_' . Lang::locale()] }} | @lang('header.title')</title>
@endsection

@section('content')

<section class="white">

  <div class="fade">
    <div class="header">
      <div class="top-pane">
        <div class="container flex-between">
          <div class="langs">
            <a href="/locale/pl"><img src="/assets/index/img/PL_flag.gif" alt=""></a>
            <a href="/locale/en"><img src="/assets/index/img/en.jpg" alt=""></a>
            <a href="/locale/ru"><img src="/assets/index/img/rus.png" alt=""></a>
          </div>
          <div class="contacts">
            <a href="">@lang('header.phone1')</a>
            <a href="">@lang('header.phone2')</a>
          </div>
        </div>
      </div>
      <div class="container flex-between padding">
        <h1 class="mainTitle">@lang('header.title')</h1>
        <span class="logo">
          <a href="/">
            <img src="/assets/index/img/logo.png" alt="">
          </a>
        </span>
        <div class="menu">
          <!--<a id="about2">@lang('header.about')</a>-->
          <a id="contacts2">@lang('header.contacts')</a>
          <a id="catalog2">@lang('header.catalog')</a>
          <a id="mail2" href=""><span class="fa fa-envelope"></span></a>
        </div>
      </div>
    </div>

    <div class="container mt-120">
      <div class="triangle"></div>
      <div class="text-center">
        <h2>{{$category['name_' . Lang::locale()]}}</h2>
        <p>@lang('main.pList')</p>
      </div>
        @foreach ($products as $product)
            <div class="card">
              <div data-popup-open="popup-{{$product->id}}" class="image-container">
                <img src="{{$product->image_url}}" alt="">
              </div>
              <div class="info">
                <h4>{{$product['name_' . Lang::locale()]}}</h4>
                <p>{{ $product['about_' . Lang::locale()] }}</p>
                <a data-popup-open="popup-{{ $product->id }}-offer">@lang('main.get')</a>
                @if($product->model_url && strlen($product->model_url) > 20)
                  <a data-popup-open="popup-{{$product->id}}" class="flat">@lang('main.preview')</a>
                @endif
              </div>
            </div>
        @endforeach
    </div>
  </div>
</section>

@foreach ($products as $product)
  <div class="popup" data-popup="popup-{{ $product->id }}-offer">
    <div class="popup-inner">
      <h2 class="offer">Заявка на услугу {{$product['name_' . Lang::locale()]}}</h2>
      <p>@lang('main.tip')</p>
      <form class="contactUs" role="form" method="post" action="/send">
        <input style="display: none;" name="product" type="text" value="{{$product['name_' . Lang::locale()]}}"/>
        <div class="input-line flex-between">
          <input name="name" type="text" placeholder="@lang('mail.name')"required>
          <input name="time" type="text" placeholder="@lang('mail.time')">
        </div>
        <div class="input-line flex-between">
          <input name="email" type="text" placeholder="@lang('mail.email')">
          <input name="number" type="tel" value="+375" placeholder="@lang('mail.phone')" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" required>
        </div>
        <div class="input-line flex-between">
          <textarea name="text" rows="4" placeholder="@lang('mail.message')"></textarea>
        </div>
        <div class="actions">
          <button class="flat" data-popup-close="popup-{{ $product->id }}-offer">@lang('main.cancel')</button>
          <button type="submit">@lang('main.get')</button>
        </div>
      </form>
      <span class="fa fa-close" data-popup-close="popup-{{ $product->id }}-offer"</span>
    </div>
  </div>
@endforeach

@foreach ($products as $product)
  <div class="popup" data-popup="popup-{{ $product->id }}">
    <div class="popup-inner">
      <h2 class="offer">{{$product['name_' . Lang::locale()]}}</h2>
      @if ($product->model_url)
        <iframe src="{{$product->model_url}}/embed?autostart=1&autospin=0.5"></iframe>
      @endif
      <span class="fa fa-close" data-popup-close="popup-{{ $product->id }}"</span>
    </div>
  </div>
@endforeach

@endsection