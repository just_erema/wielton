@extends('index.layout')

@section('head')
  <title>WILLOW | Главная | Конструкторское бюро</title>
@endsection

@section('content')

<section class="main">
  <img src="/assets/index/img/main.jpg" alt="">

  <div class="fade">
    <div class="header">
      <div class="top-pane">
        <div class="container flex-between">
          <div class="langs">
            <a href="/locale/pl"><img src="/assets/index/img/PL_flag.gif" alt=""></a>
            <a href="/locale/en"><img src="/assets/index/img/en.jpg" alt=""></a>
            <a href="/locale/ru"><img src="/assets/index/img/rus.png" alt=""></a>
          </div>
          <div class="contacts">
            <a href="">@lang('header.phone1')</a>
            <a href="">@lang('header.phone2')</a>
          </div>
        </div>
      </div>
      <div class="container flex-between padding">
        <h1 class="mainTitle">@lang('header.title')</h1>
        <span class="logo">
          <a href="/">
            <img src="/assets/index/img/logo.png" alt="">
          </a>
        </span>
        <div class="menu">
          <!--<a id="about2">@lang('header.about')</a>-->
          <a id="contacts2">@lang('header.contacts')</a>
          <a id="catalog2">@lang('header.catalog')</a>
          <a id="mail2" href=""><span class="fa fa-envelope"></span></a>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="flex-column">
        <div class="flex-between">
          <h2 class="slogan">@lang('main.slogan')</h2>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="white" id="white">
  <div class="siema">
  @foreach ($slider as $item)
    <div class="slide">
      <div>
        <a href="{{$item['slug_' . Lang::locale()]}}"><img src="{{$item->image_url}}" alt="{{$item['name_' . Lang::locale()]}}"></a>
        <div>
          <a href="{{$item['slug_' . Lang::locale()]}}"><h2>{{$item['name_' . Lang::locale()]}}</h2></a>
          <p>
            @if (count($item->children) > 0)
              @foreach($item->children as $child)
                <a href="{{$child['slug_' . Lang::locale()]}}">- {{$child['name_' . Lang::locale()]}}</a><br/>
              @endforeach
            @endif
            @if (count($item->products) > 0)
              @foreach($item->products as $product)
                <a href="{{$item['slug_' . Lang::locale()]}}">- {{$product['name_' . Lang::locale()]}}</a><br/>
              @endforeach
            @endif
          </p>
          <a href="{{$item['slug_' . Lang::locale()]}}" class="get">@lang('header.goto')</a>
        </div>
      </div>
    </div> 
  @endforeach
  </div>
</section>

<div class="color">
  <h4 class="wow slideInLeft" data-wow-delay=".5s">@lang('main.why')</h4>
  <div class="container attainments flex-between">
    <div class="wow fadeInUp attainments__inner" data-wow-delay=".3s">
      <p class="fa fa-star"></p>
      <p>@lang('main.amount')</p>
    </div>
    <div class="wow fadeInUp attainments__inner" data-wow-delay=".5s">
      <p class="fa fa-birthday-cake"></p>
      <p>@lang('main.exper')</p>
    </div>
    <div class="wow fadeInUp attainments__inner" data-wow-delay=".8s">
      <p class="fa fa-trophy"></p>
      <p>@lang('main.quality')</p>
    </div>
  </div>
</div>

<section class="paralax pad" id="aboutBlock">
  <div class="text-block container">
    <div class="wow fadeInUp"  data-wow-delay=".2s">
      <h4>@lang('main.offerTitle')</h4>
      <p>@lang('main.offer')</p>
    </div>

    <div class="wow fadeInUp"  data-wow-delay=".2s">
      <h4>@lang('main.expTitle')</h4>
      <p>@lang('main.exp')</p>
    </div>

    <div class="wow fadeInUp"  data-wow-delay=".2s">
      <h4>@lang('main.analyzTitle')</h4>
      <p>
          @lang('main.analyz')
      </p>
    </div>

    <div class="wow fadeInUp"  data-wow-delay=".2s">
      <h4>@lang('main.modernTitle')</h4>
      <p>
          @lang('main.modern')
      </p>
    </div>
  </div>
</section>

<script>
  const mySiema = new Siema({
    duration: 1000, 
    loop: true,
    draggable: false
  });

  var i = 1;
  var manual = false;

  setInterval(() => {
    if (!manual){
      if (i < mySiema.innerElements.length) {
        document.getElementById(i++).click();
        manual = false;
      } else {
        document.getElementById(0).click();
        manual = false;
        i = 1;
      }
    }
  }, 6000);

  Siema.prototype.addPagination = function() {
    for (let i = 0; i < this.innerElements.length; i++) {
      const btn = document.createElement('button');
      btn.setAttribute("id", i);
      btn.addEventListener('click', () => {
          for (let i = 0; i < this.innerElements.length; i++){
            document.getElementById(i).classList.remove('active');   
          }
          document.getElementById(i).classList.add('active');
          this.goTo(i)
          manual = true;
        });
      this.selector.appendChild(btn);
    }
    document.getElementById(0).classList.add('active');
  };

  // Trigger pagination creator
  mySiema.addPagination();

  // Previous slide
  function prevSlide(){
    mySiema.prev();
    currentPos();
  };

  // Next slide
  function nextSlide(){
    mySiema.next();
    currentPos();
  };

</script>
@endsection