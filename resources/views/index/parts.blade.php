@extends('index.layout')

@section('head')
  <title>WILLOW | {{ $category['name_' . Lang::locale()] }} | @lang('header.title')</title>
@endsection

@section('content')

<section class="white">

  <div class="fade">
    <div class="header">
      <div class="top-pane">
        <div class="container flex-between">
          <div class="langs">
            <a href="/locale/pl"><img src="/assets/index/img/PL_flag.gif" alt=""></a>
            <a href="/locale/en"><img src="/assets/index/img/en.jpg" alt=""></a>
            <a href="/locale/ru"><img src="/assets/index/img/rus.png" alt=""></a>
          </div>
          <div class="contacts">
            <a href="">@lang('header.phone1')</a>
            <a href="">@lang('header.phone2')</a>
          </div>
        </div>
      </div>
      <div class="container flex-between padding">
        <h1 class="mainTitle">@lang('header.title')</h1>
        <span class="logo">
          <a href="/">
            <img src="/assets/index/img/logo.png" alt="">
          </a>
        </span>
        <div class="menu">
          <!--<a id="about2">@lang('header.about')</a>-->
          <a id="contacts2">@lang('header.contacts')</a>
          <a id="catalog2">@lang('header.catalog')</a>
          <a id="mail2" href=""><span class="fa fa-envelope"></span></a>
        </div>
      </div>
    </div>

    <div class="container mt-120">
      <div class="triangle"></div>
      <div class="text-center">
        <h2>{{ $category['name_' . Lang::locale()] }}</h2>
        <p>@lang('main.cList')</p>
      </div>
      <ul class="list">
        @foreach ($subcategories as $child)
            <li>
                <span class="fa fa-archive"></span>
                <a class="name" href="/{{ $child['slug_' . Lang::locale()] }}"><h4>{{$child['name_' . Lang::locale()]}}</h4></a>
                @foreach($child->products as $product)
                  <a href="/{{ $child['slug_' . Lang::locale()] }}"><p>{{ $product['name_' . Lang::locale()] }}</p></a><br>
                @endforeach
                <a href="/{{ $child['slug_' . Lang::locale()] }}">@lang('header.goto') <span class="fa fa-arrow-right"></span></a>
            </li>
        @endforeach
      </ul>
      </div>
    </div>
  </div>
</section>

@endsection