<!DOCTYPE html>
<html>
  <head>
	@yield('head')
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/index/css/animate.css">
	<link rel="stylesheet" href="/assets/index/css/style.css">
	<link rel="stylesheet" href="{{ mix('/assets/index/css/app.css') }}">
	<script src="/assets/index/js/wow.min.js"></script>
	<script src="/assets/index/js/siema.min.js"></script>
	<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script src='/assets/index/js/jquery.reel-min.js' type='text/javascript'></script>
  </head>
<body>

	<header>
		<div class="top-pane">
			<div class="container flex-between">
				<div class="langs">
					<a href="/locale/pl"><img src="/assets/index/img/PL_flag.gif" alt=""></a>
					<a href="/locale/en"><img src="/assets/index/img/en.jpg" alt=""></a>
					<a href="/locale/ru"><img src="/assets/index/img/rus.png" alt=""></a>
				</div>
				<div class="contacts">
					<a href="">@lang('header.phone1')</a>
					<a href="">@lang('header.phone2')</a>
				</div>
			</div>
		</div>
		<div class="container flex-between padding">
			<h1 class="mainTitle">@lang('header.title')</h1>
			<span class="logo">
				<a href="/">
					<img src="/assets/index/img/logo.png" alt="">
				</a>
			</span>
			<div class="menu">
				<!--<a id="about">@lang('header.about')</a>-->
				<a id="contacts">@lang('header.contacts')</a>
				<a id="catalog">@lang('header.catalog')</a>
				<a id="mail" href=""><span class="fa fa-envelope"></span></a>
			</div>
		</div>
	</header>

	<div class="fon"></div>
	@yield('content')


	<footer>
		<div class="map" id="googleMap"></div>
		<div class="container flex-between">	
			<div class="address flex-between __bottom">
				<div>
					<h2 class="contactsTitle">@lang('visit.title')</h2>
					<img src="/assets/index/img/logo.png" alt="WILLOW logo">
					<p class="small">@lang('visit.adress')</p>
					<p class="small">@lang('visit.adress2')</p>
				</div>
				<div>
					<p class="right">@lang('visit.name')</p>
					<p class="small mb right">@lang('visit.role')</p>
					<p class="small right">@lang('visit.phone')</p>
					<p class="small right">@lang('visit.email')</p>
				</div>
			</div>
		</div>
		<div class="copyright">
			<p>@lang('header.copyright')</p>
			<p>@lang('header.copyright2')</p>	
		</div>
	</footer>

	<div class="catalog animated">
		<div class="container">
			<span id="close-catalog" class="fa fa-close"></span>
			<h2>@lang('header.catalog')</h2>
			<div class="hr"></div>

			@foreach ($categories as $category)
				<div class="category">
					<h4><a href="/{{ $category['slug_' . Lang::locale()] }}">{{ $category['name_' . Lang::locale()] }}</a></h4>
					@foreach ($category->children as $child)
						<a href="/{{ $child['slug_' . Lang::locale()] }}">
							<p><span class="fa fa-circle"></span>{{ $child['name_' . Lang::locale()] }}</p>
						</a>
					@endforeach
					@foreach ($category->products as $child)
						<a href="/{{ $category['slug_' . Lang::locale()] }}">
							<p><span class="fa fa-circle"></span>{{ $child['name_' . Lang::locale()] }}</p>
						</a>
					@endforeach
					<a href="/{{ $category['slug_' . Lang::locale()] }}">@lang('header.goto') <span class="fa fa-arrow-right"></span></a>
				</div>
			@endforeach
			
		</div>
	</div>

	<div class="contact-form animated">
		<div class="container">
			<span id="close-mail" class="fa fa-close"></span>
			<h2>@lang('mail.title')</h2>
			<div class="hr"></div>

			<form role="form" method="post" action="/send">
				<div class="input-line flex-between">
					<input type="text" name="name" placeholder="@lang('mail.name')">
					<input type="text" name="email" placeholder="@lang('mail.email')">
				</div>
				<div class="input-line flex-between">
					<input type="text" name="number" placeholder="@lang('mail.phone')">
					<input type="text" name="time" placeholder="@lang('mail.time')">
				</div>
				<div class="input-line flex-between">
					<textarea name="text" id="" cols="30" rows="4" placeholder="@lang('mail.message')"></textarea>
					<button type="submit">@lang('mail.send')</button>
				</div>
			</form>
		</div>
	</div>
	<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJcKXsZrXwdbXx2CM08n3AHl9frIHxU1w&callback=myMap"></script>
	<script>
		new WOW().init();
		window.onload = function() {
			$('html,body').css('opacity', '1');
			$('html,body').addClass('animated fadeIn');

			$(function() {
		    //----- OPEN
				$('[data-popup-open]').on('click', function(e)  {
					var targeted_popup_class = jQuery(this).attr('data-popup-open');
					$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
			
					e.preventDefault();
				});
			
				//----- CLOSE
				$('[data-popup-close]').on('click', function(e)  {
					var targeted_popup_class = jQuery(this).attr('data-popup-close');
					$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
			
					e.preventDefault();
				});
			});
			$("#catalog, #catalog2").click(function(e){
				e.preventDefault();
				$(".catalog").show();
				$(".catalog").addClass('slideInDown');
				setTimeout(function(){
					$(".catalog").removeClass('slideInDown');
				},1000);
			})
			$('#close-catalog').click(function(e){
				e.preventDefault();
				$(".catalog").addClass('slideOutRight');
				setTimeout(function(){
					$(".catalog").hide();
					$(".catalog").removeClass('slideOutRight');
				}, 1000);
			});
			$("#mail, #mail2").click(function(e){
				e.preventDefault();
				$(".contact-form").show();
				$(".contact-form").addClass('slideInDown');
				setTimeout(function(){
					$(".contact-form").removeClass('slideInDown');
				},1000);
			})
			$('#close-mail').click(function(e){
				e.preventDefault();
				$(".contact-form").addClass('slideOutRight');
				setTimeout(function(){
					$(".contact-form").hide();
					$(".contact-form").removeClass('slideOutRight');
				}, 1000);
			});
			var body = $("html, body");
			$("#about, #about2").click(function(e){
				e.preventDefault();
				body.stop().animate({scrollTop: $("#aboutBlock").offset().top}, 800, 'swing');
			})
			$("#contacts, #contacts2").click(function(e){
				e.preventDefault();
				body.stop().animate({scrollTop: $("#googleMap").offset().top}, 800, 'swing');
			})
		};

		function myMap() {
			var map = new google.maps.Map(document.getElementById('googleMap'), {
				center: {lat: 52.1834289, lng: 21.0578673},
				zoom: 17,
				styles: [
							{
									"featureType": "administrative",
									"elementType": "all",
									"stylers": [
											{
													"saturation": "-100"
											}
									]
							},
							{
									"featureType": "administrative.province",
									"elementType": "all",
									"stylers": [
											{
													"visibility": "off"
											}
									]
							},
							{
									"featureType": "landscape",
									"elementType": "all",
									"stylers": [
											{
													"saturation": -100
											},
											{
													"lightness": 65
											},
											{
													"visibility": "on"
											}
									]
							},
							{
									"featureType": "poi",
									"elementType": "all",
									"stylers": [
											{
													"saturation": -100
											},
											{
													"lightness": "50"
											},
											{
													"visibility": "simplified"
											}
									]
							},
							{
									"featureType": "road",
									"elementType": "all",
									"stylers": [
											{
													"saturation": "-100"
											}
									]
							},
							{
									"featureType": "road.highway",
									"elementType": "all",
									"stylers": [
											{
													"visibility": "simplified"
											}
									]
							},
							{
									"featureType": "road.arterial",
									"elementType": "all",
									"stylers": [
											{
													"lightness": "30"
											}
									]
							},
							{
									"featureType": "road.local",
									"elementType": "all",
									"stylers": [
											{
													"lightness": "40"
											}
									]
							},
							{
									"featureType": "transit",
									"elementType": "all",
									"stylers": [
											{
													"saturation": -100
											},
											{
													"visibility": "simplified"
											}
									]
							},
							{
									"featureType": "water",
									"elementType": "geometry",
									"stylers": [
											{
													"hue": "#ffff00"
											},
											{
													"lightness": -25
											},
											{
													"saturation": -97
											}
									]
							},
							{
									"featureType": "water",
									"elementType": "labels",
									"stylers": [
											{
													"lightness": -25
											},
											{
													"saturation": -100
											}
									]
							}
					]
			});
			var marker = new google.maps.Marker({
				position: {lat: 52.1834289, lng: 21.0578673},
				map: map,
				title: 'Willow'
			});
		};


		window.onscroll = function() {
  			if (window.pageYOffset > 900) {
				$('header').addClass('shadow');
			} else {
				$('header').removeClass('shadow');
			}
		}

	</script>
</body>
</html>