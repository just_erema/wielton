<?php

return [

    'about' => 'O nas',
    'contacts' => 'Kontakty',
    'catalog' => 'Katalog',
    'phone1' => '+48 884 668 378',
    'phone2' => 'Darmowe połączenie',
    'title' => 'Biuro projektowe',
    'copyright' => 'Prawo autorskie Willow 2018',
    'copyright2' => 'Wszelkie prawa zastrzeżone',
    'goto' => 'więcej informacji'
];
