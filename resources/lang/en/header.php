<?php

return [

    'about' => 'About',
    'contacts' => 'Contacts',
    'catalog' => 'Services',
    'phone1' => '+48 884 668 378',
    'phone2' => 'Call is FREE',
    'title' => 'Design department',
    'copyright' => 'Copyright Willow 2018',
    'copyright2' => 'All rights reserved',
    'goto' => 'more'
];
