<?php


use App\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('products');
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru');
            $table->string('name_en');
            $table->string('name_pl');
            $table->string('slug_ru')->nullable()->unique();
            $table->string('slug_en')->nullable()->unique();
            $table->string('slug_pl')->nullable()->unique();
            $table->text('about_ru');
            $table->text('about_en');
            $table->text('about_pl');
            $table->string('image');
            $table->integer('category_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('products', function($table) {
            $table->foreign('category_id')->references('id')->on('categories');
        });                                        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
