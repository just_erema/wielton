<?php

use App\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::dropIfExists('categories');
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru');
            $table->string('name_en');
            $table->string('name_pl');
            $table->string('slug_ru')->nullable()->unique();
            $table->string('slug_en')->nullable()->unique();
            $table->string('slug_pl')->nullable()->unique();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamps();
        });   
        Schema::table('categories', function($table) {
            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade');
        });                
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }       
}
